<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Users');
	}

	public function login()
	{
    	//$this->load->view('welcome_message');
			if(isset($_POST['submit']))
			{
				//echo "Proses Data";
				$username = $this->input->post('username');
				$password = $this->input->post('password');
				$hasil = $this->Users->login($username, $password);
				//echo $hasil;
				if($hasil==1)
				{
					$this->session->userdata(array('role'=>'Administrator'));
					redirect('Dashboard');
				}
				else
				{
					redirect('Auth/login');
				}
			}
			else
			{
				$this->load->view('welcome_message');
			}
	}

	public function logout()
	{
		session_destroy();
		redirect('/');
	}

}

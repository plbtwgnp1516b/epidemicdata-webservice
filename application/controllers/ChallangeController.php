<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class ChallangeController extends CI_Controller {

  public function __construct()
  {
    parent::__construct();
    $this->load->model('Challange');
  }

  public function getChallange($page, $size)
  {

    $response = array(
      'content' => $this->Challange->getChallange(($page - 1) * $size, $size)->result(),
      'totalPages' => ceil($this->Challange->getCountChallange() / $size));

    $this->output
      ->set_status_header(200)
      ->set_content_type('application/json', 'utf-8')
      ->set_output(json_encode($response, JSON_PRETTY_PRINT))
      ->_display();
      exit;
  }

  public function saveChallange()
  {
      $data = (array)json_decode(file_get_contents('php://input'));
      $this->Challange->insertChallange($data);

      $response = array(
        'Success' => true,
        'Info' => 'Data Tersimpan');

      $this->output
        ->set_status_header(201)
        ->set_content_type('application/json', 'utf-8')
        ->set_output(json_encode($response, JSON_PRETTY_PRINT))
        ->_display();
        exit;
  }

  public function updateChallange($id_challange)
  {
    $data = (array)json_decode(file_get_contents('php://input'));
    $this->Challange->updateChallange($data, $id_challange);

    $response = array(
      'Success' => true,
      'Info' => 'Data Berhasil di update');

    $this->output
      ->set_status_header(200)
      ->set_content_type('application/json', 'utf-8')
      ->set_output(json_encode($response, JSON_PRETTY_PRINT))
      ->_display();
      exit;
  }

  public function deleteChallange($id_challange)
  {
    $this->Challange->deleteChallange($id_challange);

    $response = array(
      'Success' => true,
      'Info' => 'Data Berhasil di hapus');

    $this->output
      ->set_status_header(200)
      ->set_content_type('application/json', 'utf-8')
      ->set_output(json_encode($response, JSON_PRETTY_PRINT))
      ->_display();
      exit;
  }

}

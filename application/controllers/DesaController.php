<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class DesaController extends CI_Controller {

  public function __construct()
  {
    parent::__construct();
    $this->load->model('Desa');
  }

  public function getDesa($page, $size)
  {

    $response = array(
      'content' => $this->Desa->getDesa(($page - 1) * $size, $size)->result(),
      'totalPages' => ceil($this->Desa->getCountDesa() / $size));

    $this->output
      ->set_status_header(200)
      ->set_content_type('application/json', 'utf-8')
      ->set_output(json_encode($response, JSON_PRETTY_PRINT))
      ->_display();
      exit;
  }

  public function saveDesa()
  {
      $data = (array)json_decode(file_get_contents('php://input'));
      $this->Desa->insertDesa($data);

      $response = array(
        'Success' => true,
        'Info' => 'Data Tersimpan');

      $this->output
        ->set_status_header(201)
        ->set_content_type('application/json', 'utf-8')
        ->set_output(json_encode($response, JSON_PRETTY_PRINT))
        ->_display();
        exit;
  }

  public function updateDesa($id_desa)
  {
    $data = (array)json_decode(file_get_contents('php://input'));
    $this->Desa->updateDesa($data, $id_desa);

    $response = array(
      'Success' => true,
      'Info' => 'Data Berhasil di update');

    $this->output
      ->set_status_header(200)
      ->set_content_type('application/json', 'utf-8')
      ->set_output(json_encode($response, JSON_PRETTY_PRINT))
      ->_display();
      exit;
  }

  public function deleteDesa($id_desa)
  {
    $this->Desa->deleteDesa($id_desa);

    $response = array(
      'Success' => true,
      'Info' => 'Data Berhasil di hapus');

    $this->output
      ->set_status_header(200)
      ->set_content_type('application/json', 'utf-8')
      ->set_output(json_encode($response, JSON_PRETTY_PRINT))
      ->_display();
      exit;
  }

}

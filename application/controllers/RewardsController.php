<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class RewardsController extends CI_Controller {

  public function __construct()
  {
    parent::__construct();
    $this->load->model('Rewards');
  }

  public function getRewards($page, $size)
  {

    $response = array(
      'content' => $this->Rewards->getRewards(($page - 1) * $size, $size)->result(),
      'totalPages' => ceil($this->Rewards->getCountRewards() / $size));

    $this->output
      ->set_status_header(200)
      ->set_content_type('application/json', 'utf-8')
      ->set_output(json_encode($response, JSON_PRETTY_PRINT))
      ->_display();
      exit;
  }

  public function saveRewards()
  {
      $data = (array)json_decode(file_get_contents('php://input'));
      $this->Rewards->insertRewards($data);

      $response = array(
        'Success' => true,
        'Info' => 'Data Tersimpan');

      $this->output
        ->set_status_header(201)
        ->set_content_type('application/json', 'utf-8')
        ->set_output(json_encode($response, JSON_PRETTY_PRINT))
        ->_display();
        exit;
  }

  public function updateRewards($id_rewards)
  {
    $data = (array)json_decode(file_get_contents('php://input'));
    $this->Rewards->updateRewards($data, $id_rewards);

    $response = array(
      'Success' => true,
      'Info' => 'Data Berhasil di update');

    $this->output
      ->set_status_header(200)
      ->set_content_type('application/json', 'utf-8')
      ->set_output(json_encode($response, JSON_PRETTY_PRINT))
      ->_display();
      exit;
  }
}

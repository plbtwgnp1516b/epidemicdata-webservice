<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class KecamatanController extends CI_Controller {

  public function __construct()
  {
    parent::__construct();
    $this->load->model('Kecamatan');
  }

  public function getKecamatan($page, $size)
  {

    $response = array(
      'content' => $this->Kecamatan->getKecamatan(($page - 1) * $size, $size)->result(),
      'totalPages' => ceil($this->Kecamatan->getCountKecamatan() / $size));

    $this->output
      ->set_status_header(200)
      ->set_content_type('application/json', 'utf-8')
      ->set_output(json_encode($response, JSON_PRETTY_PRINT))
      ->_display();
      exit;
  }

  public function saveKecamatan()
  {
      $data = (array)json_decode(file_get_contents('php://input'));
      $this->Kecamatan->insertKecamatan($data);

      $response = array(
        'Success' => true,
        'Info' => 'Data Tersimpan');

      $this->output
        ->set_status_header(201)
        ->set_content_type('application/json', 'utf-8')
        ->set_output(json_encode($response, JSON_PRETTY_PRINT))
        ->_display();
        exit;
  }

  public function updateKecamatan($id_kecamatan)
  {
    $data = (array)json_decode(file_get_contents('php://input'));
    $this->Kecamatan->updateKecamatan($data, $id_kecamatan);

    $response = array(
      'Success' => true,
      'Info' => 'Data Berhasil di update');

    $this->output
      ->set_status_header(200)
      ->set_content_type('application/json', 'utf-8')
      ->set_output(json_encode($response, JSON_PRETTY_PRINT))
      ->_display();
      exit;
  }

  public function deleteKecamatan($id_kecamatan)
  {
    $this->Kecamatan->deleteKecamatan($id_kecamatan);

    $response = array(
      'Success' => true,
      'Info' => 'Data Berhasil di hapus');

    $this->output
      ->set_status_header(200)
      ->set_content_type('application/json', 'utf-8')
      ->set_output(json_encode($response, JSON_PRETTY_PRINT))
      ->_display();
      exit;
  }

}

<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class RedeemController extends CI_Controller {

  public function __construct()
  {
    parent::__construct();
    $this->load->model('Redeem');
  }

  public function getRedeem($page, $size)
  {

    $response = array(
      'content' => $this->Redeem->getRedeem(($page - 1) * $size, $size)->result(),
      'totalPages' => ceil($this->Redeem->getCountRedeem() / $size));

    $this->output
      ->set_status_header(200)
      ->set_content_type('application/json', 'utf-8')
      ->set_output(json_encode($response, JSON_PRETTY_PRINT))
      ->_display();
      exit;
  }

  public function saveRedeem()
  {
      $data = (array)json_decode(file_get_contents('php://input'));
      $this->Redeem->insertRedeem($data);

      $response = array(
        'Success' => true,
        'Info' => 'Data Tersimpan');

      $this->output
        ->set_status_header(201)
        ->set_content_type('application/json', 'utf-8')
        ->set_output(json_encode($response, JSON_PRETTY_PRINT))
        ->_display();
        exit;
  }

  public function updateRedeem($id_redeem)
  {
    $data = (array)json_decode(file_get_contents('php://input'));
    $this->Redeem->updateRedeem($data, $id_redeem);

    $response = array(
      'Success' => true,
      'Info' => 'Data Berhasil di update');

    $this->output
      ->set_status_header(200)
      ->set_content_type('application/json', 'utf-8')
      ->set_output(json_encode($response, JSON_PRETTY_PRINT))
      ->_display();
      exit;
  }

  public function deleteRedeem($id_redeem)
  {
    $this->Redeem->deleteRedeem($id_redeem);

    $response = array(
      'Success' => true,
      'Info' => 'Data Berhasil di hapus');

    $this->output
      ->set_status_header(200)
      ->set_content_type('application/json', 'utf-8')
      ->set_output(json_encode($response, JSON_PRETTY_PRINT))
      ->_display();
      exit;
  }

}

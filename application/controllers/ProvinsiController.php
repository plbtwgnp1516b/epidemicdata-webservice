<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class ProvinsiController extends CI_Controller {

  public function __construct()
  {
    parent::__construct();
    $this->load->model('Provinsi');
  }

  public function getProvinsi($page, $size)
  {

    $response = array(
      'content' => $this->Provinsi->getProvinsi(($page - 1) * $size, $size)->result(),
      'totalPages' => ceil($this->Provinsi->getCountProvinsi() / $size));

    $this->output
      ->set_status_header(200)
      ->set_content_type('application/json', 'utf-8')
      ->set_output(json_encode($response, JSON_PRETTY_PRINT))
      ->_display();
      exit;
  }

  public function saveProvinsi()
  {
      $data = (array)json_decode(file_get_contents('php://input'));
      $this->Provinsi->insertProvinsi($data);

      $response = array(
        'Success' => true,
        'Info' => 'Data Tersimpan');

      $this->output
        ->set_status_header(201)
        ->set_content_type('application/json', 'utf-8')
        ->set_output(json_encode($response, JSON_PRETTY_PRINT))
        ->_display();
        exit;
  }

  public function updateProvinsi($id_provinsi)
  {
    $data = (array)json_decode(file_get_contents('php://input'));
    $this->Provinsi->updateProvinsi($data, $id_provinsi);

    $response = array(
      'Success' => true,
      'Info' => 'Data Berhasil di update');

    $this->output
      ->set_status_header(200)
      ->set_content_type('application/json', 'utf-8')
      ->set_output(json_encode($response, JSON_PRETTY_PRINT))
      ->_display();
      exit;
  }

  public function deleteProvinsi($id_provinsi)
  {
    $this->Provinsi->deleteProvinsi($id_provinsi);

    $response = array(
      'Success' => true,
      'Info' => 'Data Berhasil di hapus');

    $this->output
      ->set_status_header(200)
      ->set_content_type('application/json', 'utf-8')
      ->set_output(json_encode($response, JSON_PRETTY_PRINT))
      ->_display();
      exit;
  }

}

<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class PenyakitController extends CI_Controller {

  public function __construct()
  {
    parent::__construct();
    $this->load->model('Penyakit');
  }

  public function getPenyakit($page, $size)
  {

    $response = array(
      'content' => $this->Penyakit->getPenyakit(($page - 1) * $size, $size)->result(),
      'totalPages' => ceil($this->Penyakit->getCountPenyakit() / $size));

    $this->output
      ->set_status_header(200)
      ->set_content_type('application/json', 'utf-8')
      ->set_output(json_encode($response, JSON_PRETTY_PRINT))
      ->_display();
      exit;
  }

  public function savePenyakit()
  {
      $data = (array)json_decode(file_get_contents('php://input'));
      $this->Penyakit->insertPenyakit($data);

      $response = array(
        'Success' => true,
        'Info' => 'Data Tersimpan');

      $this->output
        ->set_status_header(201)
        ->set_content_type('application/json', 'utf-8')
        ->set_output(json_encode($response, JSON_PRETTY_PRINT))
        ->_display();
        exit;
  }

  public function updatePenyakit($id_penyakit)
  {
    $data = (array)json_decode(file_get_contents('php://input'));
    $this->Penyakit->updatePenyakit($data, $id_provinsi);

    $response = array(
      'Success' => true,
      'Info' => 'Data Berhasil di update');

    $this->output
      ->set_status_header(200)
      ->set_content_type('application/json', 'utf-8')
      ->set_output(json_encode($response, JSON_PRETTY_PRINT))
      ->_display();
      exit;
  }

  public function deletePenyakit($Id_penyakit)
  {
    $this->Penyakit->deletePenyakit($Id_penyakit);

    $response = array(
      'Success' => true,
      'Info' => 'Data Berhasil di hapus');

    $this->output
      ->set_status_header(200)
      ->set_content_type('application/json', 'utf-8')
      ->set_output(json_encode($response, JSON_PRETTY_PRINT))
      ->_display();
      exit;
  }

}

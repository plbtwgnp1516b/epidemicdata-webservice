<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MultimediaController extends CI_Controller {

  public function __construct()
  {
    parent::__construct();
    $this->load->model('Multimedia');
  }

  public function getMultimedia($page, $size)
  {

    $response = array(
      'content' => $this->Multimedia->getMultimedia(($page - 1) * $size, $size)->result(),
      'totalPages' => ceil($this->Multimedia->getCountMultimedia() / $size));

    $this->output
      ->set_status_header(200)
      ->set_content_type('application/json', 'utf-8')
      ->set_output(json_encode($response, JSON_PRETTY_PRINT))
      ->_display();
      exit;
  }

  public function saveMultimedia()
  {
      $data = (array)json_decode(file_get_contents('php://input'));
      $this->Multimedia->insertMultimedia($data);

      $response = array(
        'Success' => true,
        'Info' => 'Data Tersimpan');

      $this->output
        ->set_status_header(201)
        ->set_content_type('application/json', 'utf-8')
        ->set_output(json_encode($response, JSON_PRETTY_PRINT))
        ->_display();
        exit;
  }

  public function updateMultimedia($id_multimedia)
  {
    $data = (array)json_decode(file_get_contents('php://input'));
    $this->Multimedia->updateMultimedia($data, $id_multimedia);

    $response = array(
      'Success' => true,
      'Info' => 'Data Berhasil di update');

    $this->output
      ->set_status_header(200)
      ->set_content_type('application/json', 'utf-8')
      ->set_output(json_encode($response, JSON_PRETTY_PRINT))
      ->_display();
      exit;
  }

  public function deleteMultimedia($id_multimedia)
  {
    $this->Multimedia->deleteMultimedia($id_multimedia);

    $response = array(
      'Success' => true,
      'Info' => 'Data Berhasil di hapus');

    $this->output
      ->set_status_header(200)
      ->set_content_type('application/json', 'utf-8')
      ->set_output(json_encode($response, JSON_PRETTY_PRINT))
      ->_display();
      exit;
  }

}

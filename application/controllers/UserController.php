<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class UserController extends CI_Controller {

  public function __construct()
  {
    parent::__construct();
    $this->load->model('User');
  }

  public function getUser($page, $size)
  {

    $response = array(
      'content' => $this->User->getUser(($page - 1) * $size, $size)->result(),
      'totalPages' => ceil($this->User->getCountUser() / $size));

    $this->output
      ->set_status_header(200)
      ->set_content_type('application/json', 'utf-8')
      ->set_output(json_encode($response, JSON_PRETTY_PRINT))
      ->_display();
      exit;
  }

  public function saveUser()
  {
      $data = (array)json_decode(file_get_contents('php://input'));
      $this->User->insertUser($data);

      $response = array(
        'Success' => true,
        'Info' => 'Data Tersimpan');

      $this->output
        ->set_status_header(201)
        ->set_content_type('application/json', 'utf-8')
        ->set_output(json_encode($response, JSON_PRETTY_PRINT))
        ->_display();
        exit;
  }

  public function updateUser($id_users)
  {
    $data = (array)json_decode(file_get_contents('php://input'));
    $this->User->updateUser($data, $id_users);

    $response = array(
      'Success' => true,
      'Info' => 'Data Berhasil di update');

    $this->output
      ->set_status_header(200)
      ->set_content_type('application/json', 'utf-8')
      ->set_output(json_encode($response, JSON_PRETTY_PRINT))
      ->_display();
      exit;
  }
}

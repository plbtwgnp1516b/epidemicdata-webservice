<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class KabupatenController extends CI_Controller {

  public function __construct()
  {
    parent::__construct();
    $this->load->model('Kabupaten');
  }

  public function getKabupaten($page, $size)
  {

    $response = array(
      'content' => $this->Kabupaten->getKabupaten(($page - 1) * $size, $size)->result(),
      'totalPages' => ceil($this->Kabupaten->getCountKabupaten() / $size));

    $this->output
      ->set_status_header(200)
      ->set_content_type('application/json', 'utf-8')
      ->set_output(json_encode($response, JSON_PRETTY_PRINT))
      ->_display();
      exit;
  }

  public function saveKabupaten()
  {
      $data = (array)json_decode(file_get_contents('php://input'));
      $this->Kabupaten->insertKabupaten($data);

      $response = array(
        'Success' => true,
        'Info' => 'Data Tersimpan');

      $this->output
        ->set_status_header(201)
        ->set_content_type('application/json', 'utf-8')
        ->set_output(json_encode($response, JSON_PRETTY_PRINT))
        ->_display();
        exit;
  }

  public function updateKabupaten($id_kabupaten)
  {
    $data = (array)json_decode(file_get_contents('php://input'));
    $this->Kabupaten->updateKabupaten($data, $id_kabupaten);

    $response = array(
      'Success' => true,
      'Info' => 'Data Berhasil di update');

    $this->output
      ->set_status_header(200)
      ->set_content_type('application/json', 'utf-8')
      ->set_output(json_encode($response, JSON_PRETTY_PRINT))
      ->_display();
      exit;
  }

  public function deleteKabupaten($id_kabupaten)
  {
    $this->Kabupaten->deleteKabupaten($id_kabupaten);

    $response = array(
      'Success' => true,
      'Info' => 'Data Berhasil di hapus');

    $this->output
      ->set_status_header(200)
      ->set_content_type('application/json', 'utf-8')
      ->set_output(json_encode($response, JSON_PRETTY_PRINT))
      ->_display();
      exit;
  }

}

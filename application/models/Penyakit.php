<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Penyakit extends CI_Model {

  public function getCountPenyakit()
  {
      return $this->db->count_all_results('penyakit', FALSE);
  }

  public function getPenyakit($size, $page)
  {
      //return $this->db->get('penyakit', $size, $page);
      $this->db->select('penyakit.id_penyakit,nama_penyakit,jumlah_penderita,deskripsi,gejala,saran_penanganan,nama_tipe_penyakit,nama_desa,nama,url');
      $this->db->from('penyakit');
      $this->db->join('tipe_penyakit', 'tipe_penyakit.id_tipe_penyakit=penyakit.id_tipe_penyakit');
      $this->db->join('desa', 'desa.id_desa=penyakit.id_desa');
      $this->db->join('users', 'users.id_users=penyakit.id_user_app');
      $this->db->join('multimedia', 'multimedia.id_multimedia=penyakit.id_multimedia');
      $this->db->order_by('penyakit.id_penyakit','DESC');
      $query = $this->db->get('',$size, $page);

      return $query;

  }

  public function insertPenyakit($dataPenyakit)
  {
      $val = array(
        'id_penyakit' => $dataPenyakit['id_penyakit'],
        'nama_penyakit' => $dataPenyakit['nama_penyakit'],
        'jumlah_penderita' => $dataPenyakit['jumlah_penderita'],
        'deskripsi' => $dataPenyakit['deskripsi'],
        'gejala' => $dataPenyakit['gejala'],
        'saran_penanganan' => $dataPenyakit['saran_penanganan'],
        'id_desa' => $dataPenyakit['id_desa'],
        'id_tipe_penyakit' => $dataPenyakit['id_tipe_penyakit'],
        'id_user_app' => $dataPenyakit['id_user_app'],
        'id_multimedia' => $dataPenyakit['id_multimedia']
      );
      $this->db->insert('penyakit', $val);
  }

  public function updatePenyakit($dataPenyakit, $id_penyakit)
  {
    $val = array(
      'nama_penyakit' => $dataPenyakit['nama_penyakit'],
      'jumlah_penderita' => $dataPenyakit['lokasi'],
      'deskripsi' => $dataPenyakit['deskripsi'],
      'gejala' => $dataPenyakit['gejala'],
      'saran_penanganan' => $dataPenyakit['saran_penanganan'],
      'id_desa' => $dataPenyakit['id_desa'],
      'id_tipe_penyakit' => $dataPenyakit['id_tipe_penyakit'],
      'id_user_app' => $dataPenyakit['id_user_app'],
    );
    $this->db->where('id_penyakit', $id_penyakit);
    $this->db->update('penyakit', $val);
  }

  public function deletePenyakit($id_penyakit)
  {
    $val = array(
      'id_penyakit' => $id_penyakit
    );
    $this->db->delete('penyakit', $val);
  }
}

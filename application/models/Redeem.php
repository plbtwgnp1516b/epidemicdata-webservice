<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Redeem extends CI_Model {

  public function getCountRedeem()
  {
      return $this->db->count_all_results('redeem', FALSE);
  }

  public function getRedeem($size, $page)
  {
      //return $this->db->get('penyakit', $size, $page);
      $this->db->select('redeem.id_redeem,redeem_key,nama,nama_rewards');
      $this->db->from('redeem');
      $this->db->join('users', 'users.id_users=redeem.id_user_app');
      $this->db->join('rewards', 'rewards.id_rewards=redeem.id_rewards');
      $query = $this->db->get('',$size, $page);

      return $query;
  }

  public function insertRedeem($dataRedeem)
  {
      $val = array(
        'id_redeem' => $dataRedeem['id_redeem'],
        'redeem_key' => $dataRedeem['redeem_key'],
        'id_user_app' => $dataRedeem['id_user_app'],
        'id_rewards' => $dataRedeem['id_rewards']
      );
      $this->db->insert('redeem', $val);
  }

  public function updateRedeem($dataRedeem, $id_redeem)
  {
    $val = array(
      'redeem_key' => $dataRedeem['redeem_key'],
      'id_user_app' => $dataRedeem['id_user_app'],
      'id_rewards' => $dataRedeem['id_rewards']
    );
    $this->db->where('id_redeem', $id_redeem);
    $this->db->update('redeem', $val);
  }

  public function deleteRedeem($id_redeem)
  {
    $val = array(
      'id_redeem' => $id_redeem
    );
    $this->db->delete('redeem', $val);
  }
}

<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Provinsi extends CI_Model {

  public function getCountProvinsi()
  {
      return $this->db->count_all_results('provinsi', FALSE);
  }

  public function getProvinsi($size, $page)
  {
      //return $this->db->get('penyakit', $size, $page);
      $this->db->select('*');
      $this->db->from('provinsi');
      $query = $this->db->get('',$size, $page);

      return $query;
  }

  public function insertProvinsi($dataProvinsi)
  {
      $val = array(
        'id_provinsi' => $dataProvinsi['id_provinsi'],
        'nama_provinsi' => $dataProvinsi['nama_provinsi']
      );
      $this->db->insert('provinsi', $val);
  }

  public function updateProvinsi($dataProvinsi, $id_provinsi)
  {
    $val = array(
      'nama_provinsi' => $dataProvinsi['nama_provinsi']
    );
    $this->db->where('id_provinsi', $id_provinsi);
    $this->db->update('provinsi', $val);
  }

  public function deleteProvinsi($id_provinsi)
  {
    $val = array(
      'id_provinsi' => $id_provinsi
    );
    $this->db->delete('provinsi', $val);
  }
}

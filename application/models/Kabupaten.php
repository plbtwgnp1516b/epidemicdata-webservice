<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Kabupaten extends CI_Model {

  public function getCountKabupaten()
  {
      return $this->db->count_all_results('kabupaten', FALSE);
  }

  public function getKabupaten($size, $page)
  {
      //return $this->db->get('penyakit', $size, $page);
      $this->db->select('id_kabupaten,nama_kabupaten,nama_provinsi');
      $this->db->from('kabupaten');
      $this->db->join('provinsi', 'provinsi.id_provinsi=kabupaten.id_provinsi');
      $query = $this->db->get('',$size, $page);

      return $query;
  }

  public function insertKabupaten($dataKabupaten)
  {
      $val = array(
        'id_kabupaten' => $dataKabupaten['id_kabupaten'],
        'nama_kabupaten' => $dataKabupaten['nama_kabupaten'],
        'id_provinsi' => $dataKabupaten['id_provinsi']
      );
      $this->db->insert('kabupaten', $val);
  }

  public function updateKabupaten($dataKabupaten, $id_kabupaten)
  {
    $val = array(
      'nama_kabupaten' => $dataKabupaten['nama_kabupaten'],
      'id_provinsi' => $dataKabupaten['id_provinsi']
    );
    $this->db->where('id_kabupaten', $id_kabupaten);
    $this->db->update('kabupaten', $val);
  }

  public function deleteKabupaten($id_kabupaten)
  {
    $val = array(
      'id_kabupaten' => $id_kabupaten
    );
    $this->db->delete('kabupaten', $val);
  }
}

<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Multimedia extends CI_Model {

  public function getCountMultimedia()
  {
      return $this->db->count_all_results('multimedia', FALSE);
  }

  public function getMultimedia($size, $page)
  {
      //return $this->db->get('penyakit', $size, $page);
      $this->db->select('multimedia.id_multimedia,tipe_multimedia,url,nama_penyakit');
      $this->db->from('multimedia');
      $this->db->join('penyakit', 'penyakit.id_penyakit=multimedia.id_penyakit');
      $query = $this->db->get('',$size, $page);

      return $query;
  }

  public function insertMultimedia($dataMultimedia)
  {
      $val = array(
        'id_multimedia' => $dataMultimedia['id_multimedia'],
        'tipe_multimedia' => $dataMultimedia['tipe_multimedia'],
        'url' => $dataMultimedia['url'],
        'id_penyakit' => $dataMultimedia['id_penyakit']
      );
      $this->db->insert('multimedia', $val);
  }

  public function updateMultimedia($dataMultimedia, $id_multimedia)
  {
    $val = array(
      'tipe_multimedia' => $dataMultimedia['tipe_multimedia'],
      'url' => $dataMultimedia['url'],
      'id_penyakit' => $dataMultimedia['id_penyakit']
    );
    $this->db->where('id_multimedia', $id_multimedia);
    $this->db->update('multimedia', $val);
  }

  public function deleteMultimedia($id_multimedia)
  {
    $val = array(
      'id_multimedia' => $id_multimedia
    );
    $this->db->delete('multimedia', $val);
  }
}

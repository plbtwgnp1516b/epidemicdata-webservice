<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Desa extends CI_Model {

  public function getCountDesa()
  {
      return $this->db->count_all_results('desa', FALSE);
  }

  public function getDesa($size, $page)
  {
      //return $this->db->get('penyakit', $size, $page);
      $this->db->select('id_desa,nama_desa,nama_kecamatan');
      $this->db->from('desa');
      $this->db->join('kecamatan', 'kecamatan.id_kecamatan=desa.id_kecamatan');
      $query = $this->db->get('',$size, $page);

      return $query;
  }

  public function insertDesa($dataDesa)
  {
      $val = array(
        'id_desa' => $dataDesa['id_desa'],
        'nama_desa' => $dataDesa['nama_desa'],
        'id_kecamatan' => $dataDesa['id_kecamatan']
      );
      $this->db->insert('desa', $val);
  }

  public function updateDesa($dataDesa, $id_desa)
  {
    $val = array(
      'nama_desa' => $dataDesa['nama_desa'],
      'id_kecamatan' => $dataDesa['id_kecamatan']
    );
    $this->db->where('id_desa', $id_desa);
    $this->db->update('desa', $val);
  }

  public function deleteDesa($id_desa)
  {
    $val = array(
      'id_desa' => $id_desa
    );
    $this->db->delete('desa', $val);
  }
}

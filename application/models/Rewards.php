<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Rewards extends CI_Model {

  public function getCountRewards()
  {
      return $this->db->count_all_results('rewards', FALSE);
  }

  public function getRewards($size, $page)
  {
      //return $this->db->get('penyakit', $size, $page);
      $this->db->select('*');
      $this->db->from('rewards');
      $query = $this->db->get('',$size, $page);

      return $query;
  }

  public function insertRewards($dataRewards)
  {
      $val = array(
        'id_rewards' => $dataRewards['id_rewards'],
        'nama_rewards' => $dataRewards['nama_rewards'],
        'deskripsi_rewards' => $dataRewards['deskripsi_rewards'],
        'poin' => $dataRewards['poin'],
        'sisa' => $dataRewards['sisa']
      );
      $this->db->insert('rewards', $val);
  }

  public function updateRewards($dataRewards, $id_rewards)
  {
    $val = array(
      'nama_rewards' => $dataRewards['nama_rewards'],
      'deskripsi_rewards' => $dataRewards['deskripsi_rewards'],
      'poin' => $dataRewards['poin'],
      'sisa' => $dataRewards['sisa']
    );
    $this->db->where('id_rewards', $id_rewards);
    $this->db->update('rewards', $val);
  }
}

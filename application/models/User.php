<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User extends CI_Model {

  public function getCountUser()
  {
      return $this->db->count_all_results('users', FALSE);
  }

  public function getUser($size, $page)
  {
      //return $this->db->get('penyakit', $size, $page);
      $this->db->select('*');
      $this->db->from('users');
      $query = $this->db->get('',$size, $page);

      return $query;
  }

  public function insertUser($dataUser)
  {
      $val = array(
        'id_users' => $dataUser['id_users'],
        'username' => $dataUser['username'],
        'password' => $dataUser['password'],
        'role' => $dataUser['role'],
        'nama' => $dataUser['nama'],
        'email' => $dataUser['email'],
        'telp' => $dataUser['telp'],
        'poin' => $dataUser['poin']
      );
      $this->db->insert('users', $val);
  }

  public function updateUser($dataUser, $id_users)
  {
    $val = array(
      'username' => $dataUser['username'],
      'password' => $dataUser['password'],
      'nama' => $dataUser['nama'],
      'email' => $dataUser['email'],
      'telp' => $dataUser['telp']
    );
    $this->db->where('id_users', $id_users);
    $this->db->update('users', $val);
  }
}

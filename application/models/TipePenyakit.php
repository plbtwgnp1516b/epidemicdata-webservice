<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class TipePenyakit extends CI_Model {

  public function getCountTipePenyakit()
  {
      return $this->db->count_all_results('tipe_penyakit', FALSE);
  }

  public function getTipePenyakit($size, $page)
  {
      //return $this->db->get('penyakit', $size, $page);
      $this->db->select('*');
      $this->db->from('tipe_penyakit');
      $query = $this->db->get('',$size, $page);

      return $query;
  }

  public function insertTipePenyakit($dataTipePenyakit)
  {
      $val = array(
        'id_tipe_penyakit' => $dataTipePenyakit['id_tipe_penyakit'],
        'nama_tipe_penyakit' => $dataTipePenyakit['nama_tipe_penyakit']
      );
      $this->db->insert('tipe_penyakit', $val);
  }

  public function updateTipePenyakit($dataTipePenyakit, $id_tipe_penyakit)
  {
    $val = array(
      'nama_tipe_penyakit' => $dataTipePenyakit['nama_tipe_penyakit']
    );
    $this->db->where('id_tipe_penyakit', $id_tipe_penyakit);
    $this->db->update('tipe_penyakit', $val);
  }

  public function deleteTipePenyakit($id_tipe_penyakit)
  {
    $val = array(
      'id_tipe_penyakit' => $id_tipe_penyakit
    );
    $this->db->delete('tipe_penyakit', $val);
  }
}

<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Kecamatan extends CI_Model {

  public function getCountKecamatan()
  {
      return $this->db->count_all_results('kecamatan', FALSE);
  }

  public function getKecamatan($size, $page)
  {
      //return $this->db->get('penyakit', $size, $page);
      $this->db->select('id_kecamatan,nama_kecamatan,nama_kabupaten');
      $this->db->from('kecamatan');
      $this->db->join('kabupaten', 'kabupaten.id_kabupaten=kecamatan.id_kabupaten');
      $query = $this->db->get('',$size, $page);

      return $query;
  }

  public function insertKecamatan($dataKecamatan)
  {
      $val = array(
        'id_kecamatan' => $dataKecamatan['id_kecamatan'],
        'nama_kecamatan' => $dataKecamatan['nama_kecamatan'],
        'id_kabupaten' => $dataKecamatan['id_kabupaten']
      );
      $this->db->insert('kecamatan', $val);
  }

  public function updateKecamatan($dataKecamatan, $id_kecamatan)
  {
    $val = array(
      'nama_kecamatan' => $dataKecamatan['nama_kecamatan'],
      'id_kabupaten' => $dataKecamatan['id_kabupaten']
    );
    $this->db->where('id_kecamatan', $id_kecamatan);
    $this->db->update('kecamatan', $val);
  }

  public function deleteKecamatan($id_kecamatan)
  {
    $val = array(
      'id_kecamatan' => $id_kecamatan
    );
    $this->db->delete('kecamatan', $val);
  }
}

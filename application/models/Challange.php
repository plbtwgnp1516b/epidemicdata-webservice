<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Challange extends CI_Model {

  public function getCountChallange()
  {
      return $this->db->count_all_results('challange', FALSE);
  }

  public function getChallange($size, $page)
  {
      //return $this->db->get('penyakit', $size, $page);
      $this->db->select('*');
      $this->db->from('challange');
      $query = $this->db->get('',$size, $page);

      return $query;
  }

  public function insertChallange($dataChallange)
  {
      $val = array(
        'id_challange' => $dataChallange['id_challange'],
        'deskripsi' => $dataChallange['deskripsi'],
        'poin_diperoleh' => $dataChallange['poin_diperoleh']
      );
      $this->db->insert('challange', $val);
  }

  public function updateChallange($dataChallange, $id_challange)
  {
    $val = array(
      'deskripsi' => $dataChallange['deskripsi'],
      'poin_diperoleh' => $dataChallange['poin_diperoleh']
    );
    $this->db->where('id_challange', $id_challange);
    $this->db->update('challange', $val);
  }

  public function deleteChallange($id_challange)
  {
    $val = array(
      'id_challange' => $id_challange
    );
    $this->db->delete('challange', $val);
  }
}

<?php
			defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html >
  <head>
    <meta charset="UTF-8">
    <title>Epidemics Administrator</title>

    <link rel="stylesheet" href="<?php echo base_url() ?>asset/css/reset.css">
    <link rel='stylesheet prefetch' href='http://fonts.googleapis.com/css?family=Roboto:400,100,300,500,700,900'>
		<link rel='stylesheet prefetch' href='http://fonts.googleapis.com/css?family=Montserrat:400,700'>
		<link rel='stylesheet prefetch' href='https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css'>
    <link rel="stylesheet" href="<?php echo base_url() ?>asset/css/style.css">

	</head>
<body>
<div class="container">
  <div class="info">
    <h1>Epidemics Administrator</h1><span>Made by B Team PLBTW</a></span>
  </div>
</div>
<div class="form">
  <div class="thumbnail"><img src="<?php echo base_url() ?>asset/img/bio.png"/></div>
  <?php
		echo form_open('Auth/login');
	?>
		    <input type="text" name="username" placeholder="Username" />
		    <input type="password" name="password" placeholder="Password" />
		    <button type="submit" name="submit" id="btn">Sign In</button>
  </form>
</div>
<script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
<script src="<?php echo base_url() ?>asset/js/index.js"></script>
</body>
</html>

<?php
			defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>.:: Epidemics | Dashboard ::.</title>
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

  <link rel="stylesheet" href="https://cdn.datatables.net/1.10.11/css/jquery.dataTables.min.css">
  <link rel="stylesheet" href="<?php echo base_url() ?>asset/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="<?php echo base_url() ?>asset/dist/css/Admin.min.css">
  <link rel="stylesheet" href="<?php echo base_url() ?>asset/dist/css/skins/_all-skins.min.css">
  <link rel="stylesheet" href="<?php echo base_url() ?>asset/plugins/iCheck/flat/blue.css">
  <link rel="stylesheet" href="<?php echo base_url() ?>asset/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
  <link rel="stylesheet" href="<?php echo base_url() ?>asset/css/shCore.css">
  <link rel="stylesheet" href="https://cdn.datatables.net/1.10.11/css/jquery.dataTables.min.css">

  <script src="<?php echo base_url() ?>asset/plugins/jQuery/jQuery-2.1.4.min.js"></script>
  <script src="<?php echo base_url() ?>asset/bootstrap/js/bootstrap.min.js"></script>
  <script src="<?php echo base_url() ?>asset/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
  <script src="<?php echo base_url() ?>asset/plugins/slimScroll/jquery.slimscroll.min.js"></script>
  <script src="<?php echo base_url() ?>asset/plugins/fastclick/fastclick.js"></script>
  <script src="<?php echo base_url() ?>asset/dist/js/app.min.js"></script>
  <script src="https://cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
  <script src="<?php echo base_url() ?>asset/tinymce/tinymce.min.js"></script>

</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
  <header class="main-header">
    <a href="" class="logo">
      <span class="logo-mini"><b>E</b>IS</span>
      <span class="logo-lg"><b>Cpanel &nbsp Epidemics</b></span>
    </a>
    <nav class="navbar navbar-static-top" role="navigation">
      <a href="" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">

          </li>
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="<?php echo base_url() ?>asset/dist/img/adlog.png" class="user-image" alt="User Image">
              <span class="hidden-xs">Admin Epidemics</span>
            </a>
            <ul class="dropdown-menu">
              <li class="user-header">
                <img src="<?php echo base_url() ?>asset/dist/img/adlog.png" class="img-circle" alt="User Image">

                <p>
                  Epidemics Administrator
                  <small>Epidemics Information Application 2016</small>
                </p>
              </li>
              </li>
              <li class="user-footer">
                <div class="pull-left">
                  <a href="" class="btn btn-default btn-flat">Profile</a>
                </div>
                <div class="pull-right">
                  <a href="Auth/logout" class="btn btn-default btn-flat">Sign out</a>
                </div>
              </li>
            </ul>
          </li>
        </ul>
      </div>
    </nav>
  </header>
  <aside class="main-sidebar">
    <section class="sidebar">
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?php echo base_url() ?>asset/dist/img/adlog.png" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>Admin Epidemics</p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
				<?php include "menu-top.php"; ?>
    </section>
  </aside>

  <div class="content-wrapper">
    <section class="content-header">
      <h1>
        Dashboard
        <small>Control panel</small>
      </h1>
    </section>
		<!-- Main content -->
	 <section class="content">
		 <div class="row">
			 <div class="col-lg-12 col-xs-12">
				 <div class="box box-info">
					 <div class="box-header">
						 <i class="fa fa-home"></i>
						 <h3 class="box-title">Penyakit</h3>
						 <div class="pull-right box-tools">
						 </div>
					 </div>
					 <div class="box-body">
						 <div class="row">
			 <div class="col-lg-12 col-xs-12">
			 <table id="example" class="display" cellspacing="0" width="100%">
					 <thead align="center" >
					 <tr>
						 <th >Image</th>
						 <th >Nama Penyakit</th>
						 <th >Lokasi</th>
							<th>Jumlah Penderita</th>
							<th >Derkripsi</th>
							<th >Gejala</th>
							<th >Saran</th>
						 <th width="10%">Actions</th>
					 </tr>
					 </thead>
					 <tbody>

					 </tbody>
					 <tfoot>
					 <tr>
						 <th >Image</th>
 						<th >Nama Penyakit</th>
 						<th >Lokasi</th>
 						 <th>Jumlah Penderita</th>
 						 <th >Derkripsi</th>
 						 <th >Gejala</th>
 						 <th >Saran</th>
 						<th width="10%">Actions</th>
					 </tr>
					 </tfoot>
				 </table>
				 </div>
			</div>
		 </div>
	 </section>

  </div>
</div>
<script type="text/javascript" language="javascript" class="init">
  $(document).ready(function() {
    $('#example').DataTable();
  } );
</script>

<script type="text/javascript">
tinymce.init({
    selector: "#mytextarea",
    plugins: [
        "advlist autolink lists link image charmap print preview anchor pagebreak",
        "searchreplace visualblocks code fullscreen",
        "insertdatetime media table contextmenu paste"
    ],
    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
});
</script>
</body>
</html>

      <ul class="sidebar-menu">
        <li class="header">MAIN NAVIGATION</li>
        <li class="treeview">
          <a href="/EpidemicApps/Dashboard">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
          </a>
        </li>
        <li class="treeview">
              <a href="#">
                <i class="fa  fa-newspaper-o"></i> <span>Penyakit</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li class="active"><a href="/EpidemicApps/Dashboard/viewnewpenyakit"><i class="fa fa-circle-o"></i>Info Penyakit Baru</a></li>
                <li class="active"><a href="/EpidemicApps/Dashboard/viewpenyakit"><i class="fa fa-circle-o"></i>Semua Penyakit</a></li>
              </ul>
            </li>
		   <li class="treeview">
          <a href="#">
            <i class="fa fa-tag"></i> <span>Comments</span> <i class="fa fa-angle-left pull-right"></i>
          </a>
          <ul class="treeview-menu">
              <li class="active"><a href=""><i class="fa fa-circle-o"></i>All Comments</a></li>
          </ul>
        </li>
        <li class="treeview">
           <a href="#">
             <i class="fa fa-dropbox"></i> <span>Rewards</span> <i class="fa fa-angle-left pull-right"></i>
           </a>
           <ul class="treeview-menu">
               <li class="active"><a href=""><i class="fa fa-circle-o"></i>All Rewards</a></li>
           </ul>
         </li>
         <li class="treeview">
            <a href="#">
              <i class="fa fa-users"></i> <span>Users</span> <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
                <li class="active"><a href=""><i class="fa fa-circle-o"></i>All Users</a></li>
            </ul>
          </li>
      </ul>

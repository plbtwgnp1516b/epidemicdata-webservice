<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'welcome';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

$route['api/penyakit/(:num)/(:num)']['GET'] = 'PenyakitController/getPenyakit/$1/$2';
$route['api/penyakit']['POST'] = 'PenyakitController/savePenyakit';
$route['api/penyakit/(:any)']['PUT'] = 'PenyakitController/updatePenyakit/$1';
$route['api/penyakit/(:any)']['DELETE'] = 'PenyakitController/deletePenyakit/$1';

$route['api/provinsi/(:num)/(:num)']['GET'] = 'ProvinsiController/getProvinsi/$1/$2';
$route['api/provinsi']['POST'] = 'ProvinsiController/saveProvinsi';
$route['api/provinsi/(:any)']['PUT'] = 'ProvinsiController/updatePenyakit/$1';
$route['api/provinsi/(:any)']['DELETE'] = 'ProvinsiController/deleteProvinsi/$1';

$route['api/kabupaten/(:num)/(:num)']['GET'] = 'KabupatenController/getKabupaten/$1/$2';
$route['api/kabupaten']['POST'] = 'KabupatenController/saveKabupaten';
$route['api/kabupaten/(:any)']['PUT'] = 'KabupatenController/updateKabupaten/$1';
$route['api/kabupaten/(:any)']['DELETE'] = 'KabupatenController/deleteKabupaten/$1';

$route['api/kecamatan/(:num)/(:num)']['GET'] = 'KecamatanController/getKecamatan/$1/$2';
$route['api/kecamatan']['POST'] = 'KecamatanController/saveKecamatan';
$route['api/kecamatan/(:any)']['PUT'] = 'KecamatanController/updateKecamatan/$1';
$route['api/kecamatan/(:any)']['DELETE'] = 'KecamatanController/deleteKecamatan/$1';

$route['api/desa/(:num)/(:num)']['GET'] = 'DesaController/getDesa/$1/$2';
$route['api/desa']['POST'] = 'DesaController/saveDesa';
$route['api/desa/(:any)']['PUT'] = 'DesaController/updateDesa/$1';
$route['api/desa/(:any)']['DELETE'] = 'DesaController/deleteDesa/$1';

$route['api/tipepenyakit/(:num)/(:num)']['GET'] = 'TipePenyakitController/getTipePenyakit/$1/$2';
$route['api/tipepenyakit']['POST'] = 'TipePenyakitController/saveTipePenyakit';
$route['api/tipepenyakit/(:any)']['PUT'] = 'TipePenyakitController/updateTipePenyakit/$1';
$route['api/tipepenyakit/(:any)']['DELETE'] = 'TipePenyakitController/deleteTipePenyakit/$1';

$route['api/multimedia/(:num)/(:num)']['GET'] = 'MultimediaController/getMultimedia/$1/$2';
$route['api/multimedia']['POST'] = 'MultimediaController/saveMultimedia';
$route['api/multimedia/(:any)']['PUT'] = 'MultimediaController/updateMultimedia/$1';
$route['api/multimedia/(:any)']['DELETE'] = 'MultimediaController/deleteMultimedia/$1';

$route['api/users/(:num)/(:num)']['GET'] = 'UserController/getUser/$1/$2';
$route['api/users']['POST'] = 'UserController/saveUser';
$route['api/users/(:any)']['PUT'] = 'UserController/updateUser/$1';

$route['api/rewards/(:num)/(:num)']['GET'] = 'RewardsController/getRewards/$1/$2';
$route['api/rewards']['POST'] = 'RewardsController/saveRewards';
$route['api/rewards/(:any)']['PUT'] = 'RewardsController/updateRewards/$1';

$route['api/challange/(:num)/(:num)']['GET'] = 'ChallangeController/getChallange/$1/$2';
$route['api/challange']['POST'] = 'ChallangeController/saveChallange';
$route['api/challange/(:any)']['PUT'] = 'ChallangeController/updateChallange/$1';
$route['api/challange/(:any)']['DELETE'] = 'ChallangeController/deleteChallange/$1';

$route['api/redeem/(:num)/(:num)']['GET'] = 'RedeemController/getRedeem/$1/$2';
$route['api/redeem']['POST'] = 'RedeemController/saveRedeem';
$route['api/redeem/(:any)']['PUT'] = 'RedeemController/updateRedeem/$1';
$route['api/redeem/(:any)']['DELETE'] = 'RedeemController/deleteRedeem/$1';
